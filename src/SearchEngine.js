import React, {Component} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";

export class SearchEngine extends Component {

    constructor() {
        super();

        this.state = {
            searchValue: "",
            links: [],
            ads: []
        }
    }

    search(searchTerm) {
        fetch("http://34.77.193.221/?query=" + searchTerm,  {mode: 'cors'})
            .then(response => response.json())
            .then(json => {
                console.log(json)
                let linkList = json.webpages.map(wp => {
                    return (
                        <div>
                            <Link href={wp.uri}>
                                {wp.title}
                            </Link>
                        </div>
                    );
                });

                this.setState({
                    links: linkList,
                    ads: json.ads
                })
            });
    }

    handleChange(e) {
        this.setState({
            searchValue: e.target.value
        });
    }

    render() {
        return (
            <div className="row" >
                <div className="col-md-6 col-md-offset-3" style={{
                    position: 'absolute', left: '50%', top: '20%',
                    transform: 'translate(-50%, -50%)'
                }}>
                    <TextField
                        id="standard-basic"
                        label="TYPE HERE"
                        inputProps={{min: 0, style: {textAlign: "center"}}}
                        value={this.state.searchValue} onChange={e => this.handleChange(e)}
                    />
                    <Button variant="contained" color="primary" onClick={() => this.search(this.state.searchValue)} style={{
                        right: '20%$'
                    }}>
                        Search
                    </Button>
                </div>

                <div style={{
                    position: 'absolute', left: '50%', top: '60%',
                    transform: 'translate(-20%, -20%)'
                }}>
                    <Typography>
                        {this.state.links}
                    </Typography>
                </div>

                {this.state.ads &&
                <div>
                <h2>Adverts</h2>
                <ul>
                    {this.state.ads.map(ad => <Link>{ad.advert}</Link>)}
                </ul>
                </div>
                }
            </div>
        );
    }
}
