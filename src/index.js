import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import {SearchEngine} from "./SearchEngine";

function App() {
  return (
    <div class="row">
        <SearchEngine></SearchEngine>
    </div>
  );
}

ReactDOM.render(<App />, document.querySelector("#app"));
